# wolfpack-stage

## Build Setup

```bash
# install dependencies
$ npm install

# add an .env file and add to it the following:
API_URL=https://join.wolfpackit.nl/api/v1/
AUTH_TOKEN=YOUR-AUTH-TOKEN

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
